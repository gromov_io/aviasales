import {sortBy} from 'lodash'

import data from './data'
export default {
	state: {
		allTickets: data.tickets,
		currency: {
			rub: {val: null, title: 'RUB', icon: '₽', key: 'rub'},
			usd: {val: 68.81, title: 'USD', icon: '$', key: 'usd'},
			eur: {val: 78.33, title: 'EUR', icon: '€', key: 'eur'}
		},
		activeCurrency: 'rub',
		filter: []
	},
	mutations: {
		SET_CURRENCY (state, key) {
			state.activeCurrency = key
		},
		SET_FILTER (state, data) {
			state.filter = data
		}
	},
	getters: {
		tickets (state) {
			if (!state.filter.length) return state.allTickets
				const result = state.allTickets.filter(item => ~state.filter.indexOf(item.stops))
			return sortBy(result, ['stops'])
		},
		getCurrentCurrencyPrice: state => priceRub => {
			const currency = state.currency[state.activeCurrency]
			if (currency.val === null) {
				return priceRub +' '+ currency.icon
			}

			const result = priceRub / currency.val
			const normalizeResult = Math.round(result * 100) / 100
			return normalizeResult +' '+ currency.icon
		}
	}
}