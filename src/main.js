import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex';
import aviasales from './store'

Vue.config.productionTip = false
Vue.use(Vuex)

Vue.prototype.$store = new Vuex.Store({
  modules: {
    aviasales
  },
})


new Vue({
  render: h => h(App)
}).$mount('#app')
